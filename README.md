# Marvel App
O projeto foi implementado de forma nativa para iOS com Swift 4. A arquitetura utilizada na  implementação foi a MVC.

As informações contidas nele foram obtidas a partir da  API da Marvel.

Foi utilizado o Cocoapods para gerenciar as dependências do projeto. O pacote adicionado foi SwiftHash para realizar criptografia.

O código foi comentado utlizando o Markup.

### Requisitos
Para a devida execução do projeto é necesário executar "pod install" via terminal.

É necessário também adicionar uma chave pública e privada, criada no site da MArvel, via código na classe Service.

No Xcode é necessário abrir o Marvel.xcworkspace.

### UI/UX
Tentou-se trazer um pouco do mundo dos quadrinhos e da Marvel na interface da aplicação.

### Execução
A primeira tela da aplicação apresenta, na forma de grade, uma lista de personagens da Marvel com o seu devido nome e foto. Ao clicar em algum deles, a aplicação apresenta uma tela de detalhes na qual consta a foto do personagem, o seu nome no topo da tela, uma descricação do personagem e, por fim, uma lista de Comics que o personagem aparece.

### Testes Unitários
Não foram realizados testes unitários.

