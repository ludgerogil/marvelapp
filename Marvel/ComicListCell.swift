//
//  ComicListCell.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 21/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//


import UIKit

/// Representa uma celula que contem um Comic com titulo e imagem
class ComicListCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var comicImage: UIImageView!
    @IBOutlet weak var comicName: UILabel!
    
    // MARK: - View
    /// Setup Appereance
    func configureCell() {
        comicName.adjustsFontSizeToFitWidth = true
        comicName.layer.borderWidth = 1.5
        comicName.layer.masksToBounds = false
        comicName.layer.borderColor = UIColor.white.cgColor
        comicName.clipsToBounds = true
        
        comicImage.layer.borderWidth = 1.5
        comicImage.layer.masksToBounds = false
        comicImage.layer.borderColor = UIColor.white.cgColor
        comicImage.clipsToBounds = true
    }
    
    
}
