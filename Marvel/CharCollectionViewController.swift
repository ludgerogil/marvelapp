//
//  CharCollectionViewController.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 17/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import UIKit

/// Representa a controller da primeira tela
class CharCollectionViewController: UICollectionViewController {
    
    // MARK: - Outlets
    @IBOutlet var charsCollectionView: UICollectionView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    // MARK: - Variaveis
    var chars: [Character] = []
    var idCharSelected: Int?
    var currentPage: Int = 0

    // MARK: - Main
    override func viewDidLoad() {
        super.viewDidLoad()
        getCharacters()
        self.navigationItem.title = "Characters"
    }

    /// Realiza a conexao com o WebService e faz a serializacao dos objetos os colocando num array
    func getCharacters() {
        guard let pathURL = URL(string: Service.charURL(page: currentPage)) else { return }
        print(pathURL)
        activity.startAnimating()
        URLSession.shared.dataTask(with: pathURL) { (data, response, error) in
            guard let data = data else { return }
            do {
                let charData = try JSONDecoder().decode(CharacterInfo.self, from: data)
                DispatchQueue.main.sync {
                    if let lista = charData.data?.results {
                        for i in lista {
                            self.chars.append(i)
                        }
                    }
                    self.charsCollectionView.reloadData()
                    self.activity.stopAnimating()
                }
            } catch let error {
                print("Erro de decodificacao: ", error)
                self.activity.stopAnimating()
            }
            } .resume()
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            //if self.chars.count < totalComic {
            currentPage = currentPage + 1
            self.getCharacters()
            //}
        }
    }
    
    
    // MARK: - DataSource e Delegates
    /// Numeros de itens da colecao
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chars.count
    }
    
    /// Conteúdo de cada celula
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let url = URL(string: (chars[indexPath.row].thumbnail?.url)!)
        let data = try? Data(contentsOf: url!)
        let image: UIImage = UIImage(data: data!)!
        
        /// adiciona as informacos nos campos da celula
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "charCell", for: indexPath) as! CharacterListCell
        cell.nameChar.text = "\(chars[indexPath.row].name!)"
        cell.imageChar.image = image
        cell.configureCell()
    
        return cell
    }
    
    /// Realiza a segue para a proxima tela ao clicar em algum item.
    override func collectionView(_ tableView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let idCharSelecionado = chars[indexPath.row]
        idCharSelected = indexPath.row
        
        self.performSegue(withIdentifier: "segueToDetails", sender: idCharSelecionado)
    }
    
    /// Envia para a proxima tela o Character escolhido ao ser clicado
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetails", let charDetail = segue.destination as? CharDetailViewController {
            charDetail.charSelected = chars[idCharSelected!]
            
        }
    }
    
    

}

