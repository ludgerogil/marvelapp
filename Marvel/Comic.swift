//
//  Comic.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 17/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import Foundation

/// Representa um objeto Comic
struct Comic: Codable {
    
    let id: Int?
    let title: String?
    let thumbnail: Thumbnail?
    
}

/// Array de Comics
struct ComicData: Codable {
    
    let total: Int?
    let results: [Comic]?
    
}

/// Representa um objeto array de comics
struct ComicInfo: Decodable {
    
    let data: ComicData?
    
}
