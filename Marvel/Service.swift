//
//  Service.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 18/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import Foundation
import SwiftHash

/// Classe com as chaves, criptografia das chaves e funcoes para criacao das URL
class Service {
    
    // MARK: - Variaveis
    static private let urlPath = "http://gateway.marvel.com/v1/public/"
    static private let privateKey = "0c4e902295f58a065986f2964126af72daf7aa92"
    static private let publicKey = "d443dec3aed373a64fd64bd5a6882996"
    static private let limit = 25
    
     // MARK: - Funcoes da classe
    /// Criacao da URL para bsucar os Characters
    class func charURL(page: Int) -> String{
        let offset = page * limit
        let url = urlPath + "characters?"  + "offset=\(offset)&limit=\(limit)&" +  getCredentials()
        return url
    }

    /// Criacao da URL para bsucar os Comics de um Character especifico
    class func comicsURL(idChar: Int) -> String{
        let id = String(idChar)
        let url = urlPath + "characters/" + id + "/comics?" + getCredentials()
        return url
        
    }
    
    /// Criptografia das Chaves com o MD5
    private class func getCredentials() -> String {
        let ts = String(Date().timeIntervalSince1970)
        let hash = MD5(ts+privateKey+publicKey).lowercased()
         return "ts=\(ts)&apikey=\(publicKey)&hash=\(hash)"
    }
    
    
    
    
    
    
    
    
    
}
