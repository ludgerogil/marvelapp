//
//  CharacterListCell.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 20/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import UIKit

/// Representa uma celula que contem um Character com nome e imagem
class CharacterListCell: UICollectionViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var nameChar      : UILabel!
    @IBOutlet weak var imageChar     : UIImageView!
    
    // MARK: - View
    /// Setup Appereance
    func configureCell() {
        nameChar.adjustsFontSizeToFitWidth = true
        nameChar.layer.borderWidth = 1.5
        nameChar.layer.masksToBounds = false
        nameChar.layer.borderColor = UIColor.white.cgColor
        nameChar.clipsToBounds = true
        
        imageChar.layer.borderWidth = 1.5
        imageChar.layer.masksToBounds = false
        imageChar.layer.borderColor = UIColor.white.cgColor
        imageChar.clipsToBounds = true
    }
    
}
