//
//  Thumbnail.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 18/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import Foundation

/// Representa uma Imagem
struct Thumbnail: Codable {
    
    let path: String
    let ext: String
    /// url = path + extensao
    var url: String
    {
        return path + "." + ext
    }
    
    enum CodingKeys: String, CodingKey
    {
        case path
        case ext = "extension"
    }
}
