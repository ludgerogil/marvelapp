//
//  Character.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 17/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import Foundation

/// Representa um objeto Character
struct Character: Decodable {
    
    let id: Int?
    let name: String?
    let description: String?
    let thumbnail: Thumbnail?
    
}
/// Array de Character
struct CharacterData: Decodable {
    
    let total: Int?
    let results: [Character]?
    
}

/// Representa um objeto arry de characters
struct CharacterInfo: Decodable {
    
    let data: CharacterData?
    
}
