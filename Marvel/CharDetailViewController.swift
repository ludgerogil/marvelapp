//
//  CharDetailViewController.swift
//  Marvel
//
//  Created by Ludgero Mascarenhas on 21/10/18.
//  Copyright © 2018 Ludgero Mascarenhas. All rights reserved.
//

import UIKit

/// Representa a controller da tela de detalhes
class CharDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Outlets
    @IBOutlet weak var charImage: UIImageView!
    @IBOutlet weak var charDescription: UITextView!
    @IBOutlet weak var comicLabel: UILabel!
    @IBOutlet weak var comicCollectionView: UICollectionView!
    @IBOutlet weak var charDescHeight: NSLayoutConstraint!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    // MARK: - Variaveis
    var charSelected: Character?
    var comics: [Comic] = []
    
    // MARK: - Main
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAppearence()
        
        comicCollectionView.delegate = self
        comicCollectionView.dataSource = self
        getComics()
    }
    /// Realiza a conexao com o WebService e faz a serializacao dos objetos os colocando num array
    func getComics() {
        guard let pathURL = URL(string: Service.comicsURL(idChar: (charSelected?.id)!)) else { return }
        print(pathURL)
        activity.startAnimating()
        URLSession.shared.dataTask(with: pathURL) { (data, response, error) in
            guard let data = data else { return }
            do {
                let comicData = try JSONDecoder().decode(ComicInfo.self, from: data)
                DispatchQueue.main.sync {
                    print("aqui 1")
                    if let lista = comicData.data?.results {
                        print("aqui 2")
                        for i in lista {
                            self.comics.append(i)
                        }
                    }
                    self.comicCollectionView.reloadData()
                    self.activity.stopAnimating()
                }
                print(self.comics.count)
            } catch let error {
                print("Erro de decodificacao: ", error)
            }
            } .resume()
    }
    
    /// Adiciona as informacoes na tela e altera o tamanho do textview de acordo com a descricao
    func setUpAppearence() {
        self.navigationItem.title = charSelected?.name
        
        if (charSelected?.description == nil || charSelected?.description == "") {
            charDescription.text = "Character has no description."
            charDescHeight.constant = 30
        }
        else {
            charDescription.text = charSelected?.description
            charDescHeight.constant = 130
        }
        
        charDescription.layer.borderWidth = 1.5
        charDescription.layer.masksToBounds = false
        charDescription.layer.borderColor = UIColor.white.cgColor
        charDescription.clipsToBounds = true
        
        comicLabel.layer.borderWidth = 1.5
        comicLabel.layer.masksToBounds = false
        comicLabel.layer.borderColor = UIColor.white.cgColor
        comicLabel.clipsToBounds = true
        
        let url = URL(string: (charSelected?.thumbnail?.url)!)
        let data = try? Data(contentsOf: url!)
        let image: UIImage = UIImage(data: data!)!
        charImage.image = image
        
        charImage.layer.borderWidth = 1.5
        charImage.layer.masksToBounds = false
        charImage.layer.borderColor = UIColor.black.cgColor
        charImage.clipsToBounds = true
    }
    
    // MARK: - DataSource e Delegates
    /// Numeros de itens da colecao
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comics.count
    }
    /// Conteúdo de cada celula
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let url = URL(string: (comics[indexPath.row].thumbnail?.url)!)
        let data = try? Data(contentsOf: url!)
        let image: UIImage = UIImage(data: data!)!
        
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "comicCell", for: indexPath) as! ComicListCell
        cell.comicName.text = "\(comics[indexPath.row].title!)"
        cell.comicImage.image = image
        cell.configureCell()
        return cell
    }
    
    
    
}
